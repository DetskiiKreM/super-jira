variable "username" {
  type = string
  description = "Username for the VKCS provider"
}

variable "vk_password" {
  type = string
  description = "Password for the VKCS provider"
}

variable "project_id" {
  type = string
  description = "Project_id for the VKCS provider"
}

variable "image_flavor" {
  type = string
  default = "Ubuntu-20.04.1-202008"
}

variable "key_pair_name" {
  type = string
  default = "GB-VM-Deploy-A4qSYnzi"
}

variable "availability_zone_name" {
  type = string
  default = "MS1"
}

variable "vm_count_jira" {
  description = "Number of VMs to create"
  type        = number
  default     = 2
}

variable "vm_count" {
  description = "Number of VMs to create"
  type        = number
  default     = 1 # Меняем на нужное кол-во для нас
}

variable "vm_names_jira" {
  description = "Names of the app VMs"
  type        = list(string)
  default     = ["jira1", "jira2"]
}

variable "vm_names" {
  description = "Names of the others VMs"
  type        = list(string)
  default     = ["mon"] # Тут прописываем имена для наших ВМ
}

variable "db_user_password" {
  type      = string
  sensitive = true
}
