resource "local_file" "ansible_inventory" {
  filename = "../ansible/inventory_tf"
  content = <<EOF
[jira]
${join("\n", vkcs_compute_instance.jira[*].network[0].fixed_ip_v4)}

[mon]
${join("\n", vkcs_compute_instance.basic[*].network[0].fixed_ip_v4)}

[cadvisor]
${join("\n", vkcs_compute_instance.jira[*].network[0].fixed_ip_v4)}

[node_exporter]
${join("\n", vkcs_compute_instance.jira[*].network[0].fixed_ip_v4)}

[db]
${join("\n", data.vkcs_networking_port.loadbalancer-port.all_fixed_ips)}
EOF
}
