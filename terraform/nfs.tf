resource "vkcs_sharedfilesystem_sharenetwork" "sharenetwork" {
  name              = "nfs_sharenetwork"
  description       = "nfs share network"
  neutron_net_id    = data.vkcs_networking_network.network.id
  neutron_subnet_id = data.vkcs_networking_subnet.subnetwork.id
}

resource "vkcs_sharedfilesystem_share" "share" {
  name             = "nfs_share"
  description      = "NFS share"
  share_proto      = "NFS"
  size             = 10
  share_network_id = vkcs_sharedfilesystem_sharenetwork.sharenetwork.id
}

resource "vkcs_sharedfilesystem_share_access" "share_access" {
  count       = var.vm_count_jira
  share_id    = vkcs_sharedfilesystem_share.share.id
  access_type = "ip"
  access_to   = element(vkcs_compute_instance.jira[*].network[0].fixed_ip_v4, count.index)
  access_level = "rw"
}
